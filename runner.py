import sys
import os
#################################
N = int(sys.argv[1])
M = int(sys.argv[2])
g = float(sys.argv[3])
aqw = float(sys.argv[4])
vqw = float(sys.argv[5])
nameWhat = sys.argv[10]
startingPsi = sys.argv[11]
#################################

d=1/float(M)
J = M**2/2
mu = M**2-g*M/2 -vqw
U = []

for q in range(0,int(aqw/d+1+0.5)+1):
    if q <= aqw/2/d-1:
        U.append(-vqw)
    elif aqw/2/d-1< q and q <= aqw/2/d+1:
        U.append(-vqw*(q+aqw/2/d-1/2))
    else:
        break     
U[0] += g*M/2

if len(U)> 8:
    print("\n\nWARNING! Really long-range interactions!!!!!!!\n\n")
##################################


command = '../job N{i}=' +sys.argv[1] \
        + ' M{i}='+sys.argv[2] \
        + ' maxOcc{i}='+sys.argv[6] \
        + ' minDim{i}='+sys.argv[7] \
        + ' maxDim{i}='+sys.argv[8] \
        + ' cutoff{d}='+sys.argv[9] \
        + ' J{d}='+str(J) \
        + ' mu{d}='+str(mu)

for i in range(len(U)):
    command += ' U'+str(i)+'{d}='+str(U[i]) 

name = ""
if "N" in nameWhat:
    name += "N"+str(N)+"_"
if "M" in nameWhat:
    name += "M"+str(M)+"_"
if "g" in nameWhat:
    name += "g"+str(g)+"_"
if "aqw" in nameWhat:
    name += "aqw"+str(aqw)+"_"
if "vqw" in nameWhat:
    name += "vqw"+str(vqw)+"_"
if "maxOcc" in nameWhat:
    name += "maxOcc"+str(sys.argv[6])+"_"
if "minDim" in nameWhat:
    name += "minDim"+str(sys.argv[7])+"_"
if "maxDim" in nameWhat:
    name += "maxDim"+str(sys.argv[8])+"_"
if "cutoff" in nameWhat:
    name += "cutoff"+str(sys.argv[9])+"_"

command += " startingPsi{b}="+startingPsi
command += " name{s}="+name
command += "  > "+name+".out"
      
#print(command)
os.system(command)

#parameters: "N,M,g,a,vqw, maxOcc, minDim, maxDim, cutoff, nameWhat"

#python paramCalcer.py 2 24 1  0.3 0.0 2 200 20 1E-8 NmaxOcc

#include "main.h"
#include <ctime>

inline bool mfileExists(const std::string name) {
    std::ifstream f(name.c_str());
    return f.good();
}

void writeObs(const Boson &sites, const MPS &psi, int sweepNum){
    auto H = hamiltonian(sites);
    auto Ell = obsEll(sites);
    auto N = obsN(sites);
    auto N_1xM = obsN_1xM(sites);
//auto AcAi = obsAA(sites);
    auto NcNi = obsNN(sites);

    std::cout << "Sweep=\t" << sweepNum << "\t";
    std::cout << "rtime=\t" << clock()/CLOCKS_PER_SEC << "\t";
    std::cout << "dim=\t" << maxLinkDim(psi) << "\t";

    std::cout << "H=\t" <<calc(H,psi) << "\t";
    std::cout << "Ell=\t" <<calc(Ell,psi) << "\t";
    std::cout << "N=\t" <<calc(N,psi) << "\t";

    std::cout << "Ni=\t";
    for(auto &ni : N_1xM){
        std::cout << calc(ni,psi) << "\t";
    }

    std::cout << "NcNi=\t";
    for(auto &ncni : NcNi){
        std::cout << calc(ncni,psi) << "\t";
    }
    std::cout << std::endl;
}

void saveIfNeeded(Boson &sites, MPS &psi,int sweepNum,Args const& args = Args::global()){
    if(sweepNum%args.getInt("nSweepsSave")==(args.getInt("nSweepsSave")-1)){
        writeToFile(args.getString("name")+"_sites",sites);
        writeToFile(args.getString("name")+"_psi",psi);
    }
}

Boson loadSitesOrCreateNew(Args const& args = Args::global()){
    if( args.getBool("startingPsi")==true){
        Boson sites;
        readFromFile( "starting_sites",sites);
        return sites;
    }
    if( mfileExists(args.getString("name")+"_sites") ){
        Boson sites;
        readFromFile( args.getString("name")+"_sites",sites);
        return sites;
    }
    return Boson(args.getInt("M"));
}


MPS loadPsiOrCreateNew(Boson sites,Args const& args = Args::global()){
    if( args.getBool("startingPsi")==true){
        MPS psi(sites);
        readFromFile( "starting_psi" ,psi);
        return psi;
    }
    if( mfileExists(args.getString("name")+"_psi") ){
        MPS psi(sites);
        readFromFile( args.getString("name")+"_psi" ,psi);
        return psi;
    }
    return genPsi0(sites);
}


void calculate(Args const& args = Args::global()){
    auto sweep = Sweeps(1,args.getInt("minDim"),args.getInt("maxDim"),args.getReal("cutoff"));
    Boson sites = loadSitesOrCreateNew();
    MPS psi = loadPsiOrCreateNew(sites);
    MPO H = hamiltonian(sites);
    for(int sweepNum=0;sweepNum<20000; sweepNum++){
        dmrg(psi,H,sweep,{"Quiet=",true,"Silent=",true});
        writeObs(sites,psi,sweepNum);
        saveIfNeeded(sites,psi,sweepNum);
    }
}



int main(int argc, char *argv[]){
    addArgs(argc,argv);

    addArg("M",96);
    addArg("N",2);
    addArg("PBC",true);

    addArg("J",4608.0);
    addArg("gomega",0.0);
    addArg("U0",0.0);
    addArg("U1",0.0);
    addArg("U2",0.0);
    addArg("U3",0.0);
    addArg("U4",0.0);
    addArg("U5",0.0);
    addArg("U6",0.0);
    addArg("U7",0.0);
    addArg("U8",0.0);
    addArg("U9",0.0);
    addArg("U10",0.0);
    addArg("U11",0.0);
    addArg("mu",0.0);

    addArg("ConserveQNs",true);
    addArg("ConserveNb",true);
    addArg("MaxOcc",2);


    addArg("minDim",40);
    addArg("maxDim",200);
    addArg("cutoff",1E-8);
    addArg("nSweepsSave",8);
    addArg("name","");
    addArg("startingPsi",false);

    calculate();

    return 0;
}
